<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>tenzor</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/normalize/3.0.3/normalize.min.css">

    <style>
        html {
            background-color: #D8DBE2;
            color: #f3f3f3;
        }

        .wrap {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            max-width: 450px;
            min-width: 300px;
            padding: 2rem;
            box-sizing: border-box;
            border-radius: 30px;
            background-color: #1B1B1E;
            overflow: hidden;
        }

        .form-name {
            font-size: 3rem;
            text-align: center;
            font-weight: 400;
            line-height: 1;

        }

        input {
            box-sizing: border-box;
            width: 100%;
            padding: 0 0 5px 0;
            margin-top: 30px;
            font-size: 1.5rem;
            background-color: transparent;
            border-top: 0;
            border-right: 0;
            border-left: 0;
            border-bottom: 1px solid #777575;
        }

        input:focus {
            outline: 0;
        }

        button {
            display: inline-block;
            width: 100%;
            padding: 10px;
            border: 0;
            margin-top: 30px;
            box-sizing: border-box;
            text-align: center;
            font-size: 1.25rem;
            text-transform: uppercase;
            line-height: 1;
            background-color: transparent;
        }

        button:focus {
            outline: 0;
        }

        .note {
            position: absolute;
            left: 30px;
            bottom: 10px;
            font-size: 10px;
            color: #777575;
        }

        @media all and (max-width: 600px) {
            .wrap {
                padding: 1rem;
            }

            .form-name {
                font-size: 2rem;
            }

            input {
                font-size: 1rem;
            }

            .note {
                display: none;
            }

            sup {
                display: none;
            }
        }

        @media all and (min-width: 601px) {
            .wrap {
                min-width: 450px;
            }
        }

        .loader-wrap {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(0,0,0,.5);
            display: none;
        }

        .sk-fading-circle {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            width: 40px;
            height: 40px;
        }

        .sk-fading-circle .sk-circle {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }

        .sk-fading-circle .sk-circle:before {
            content: '';
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            background-color: #f3f3f3;
            border-radius: 100%;
            -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
            animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
        }
        .sk-fading-circle .sk-circle2 {
            -webkit-transform: rotate(30deg);
            -ms-transform: rotate(30deg);
            transform: rotate(30deg);
        }
        .sk-fading-circle .sk-circle3 {
            -webkit-transform: rotate(60deg);
            -ms-transform: rotate(60deg);
            transform: rotate(60deg);
        }
        .sk-fading-circle .sk-circle4 {
            -webkit-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
        }
        .sk-fading-circle .sk-circle5 {
            -webkit-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
            transform: rotate(120deg);
        }
        .sk-fading-circle .sk-circle6 {
            -webkit-transform: rotate(150deg);
            -ms-transform: rotate(150deg);
            transform: rotate(150deg);
        }
        .sk-fading-circle .sk-circle7 {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        .sk-fading-circle .sk-circle8 {
            -webkit-transform: rotate(210deg);
            -ms-transform: rotate(210deg);
            transform: rotate(210deg);
        }
        .sk-fading-circle .sk-circle9 {
            -webkit-transform: rotate(240deg);
            -ms-transform: rotate(240deg);
            transform: rotate(240deg);
        }
        .sk-fading-circle .sk-circle10 {
            -webkit-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg);
        }
        .sk-fading-circle .sk-circle11 {
            -webkit-transform: rotate(300deg);
            -ms-transform: rotate(300deg);
            transform: rotate(300deg);
        }
        .sk-fading-circle .sk-circle12 {
            -webkit-transform: rotate(330deg);
            -ms-transform: rotate(330deg);
            transform: rotate(330deg);
        }
        .sk-fading-circle .sk-circle2:before {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }
        .sk-fading-circle .sk-circle3:before {
            -webkit-animation-delay: -1s;
            animation-delay: -1s;
        }
        .sk-fading-circle .sk-circle4:before {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }
        .sk-fading-circle .sk-circle5:before {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }
        .sk-fading-circle .sk-circle6:before {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s;
        }
        .sk-fading-circle .sk-circle7:before {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s;
        }
        .sk-fading-circle .sk-circle8:before {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s;
        }
        .sk-fading-circle .sk-circle9:before {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s;
        }
        .sk-fading-circle .sk-circle10:before {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s;
        }
        .sk-fading-circle .sk-circle11:before {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s;
        }
        .sk-fading-circle .sk-circle12:before {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s;
        }

        @-webkit-keyframes sk-circleFadeDelay {
            0%, 39%, 100% { opacity: 0; }
            40% { opacity: 1; }
        }

        @keyframes sk-circleFadeDelay {
            0%, 39%, 100% { opacity: 0; }
            40% { opacity: 1; }
        }
    </style>
</head>
<body>

<div class="wrap">
    <div class=form-name>Формочка<sup>*</sup></div>
    <form class="js-form" action="mail.php" method="post" autocomplete="off">
        <input type="email" name="email" placeholder="Email" autofocus required>
        <button class="js-btn" type="submit">тык</button>
        <div class="note"><em>*Минздрав предупреждает - использовать строго по назначению</em></div>

        <div class="loader-wrap js-loader">
            <div class="sk-fading-circle">
                <div class="sk-circle1 sk-circle"></div>
                <div class="sk-circle2 sk-circle"></div>
                <div class="sk-circle3 sk-circle"></div>
                <div class="sk-circle4 sk-circle"></div>
                <div class="sk-circle5 sk-circle"></div>
                <div class="sk-circle6 sk-circle"></div>
                <div class="sk-circle7 sk-circle"></div>
                <div class="sk-circle8 sk-circle"></div>
                <div class="sk-circle9 sk-circle"></div>
                <div class="sk-circle10 sk-circle"></div>
                <div class="sk-circle11 sk-circle"></div>
                <div class="sk-circle12 sk-circle"></div>
            </div>
        </div>
    </form>
</div>


<script src="https://cdn.jsdelivr.net/g/jquery@2.2.3,jquery.form@3.51"></script>

<script>
    'use strict';

    $(function () {
        var $form = $('.js-form');
        var $loader = $('.js-loader');
        var $btn = $('.js-btn');

        if ($form.length) {
            $form.ajaxForm({
                dataType: 'json',
                beforeSubmit: function() {
                    $loader.css('display', 'block');
                },
                success: function() {
                    $loader.css('display', 'none');
                    $btn.text('Всё гуд!');

                    setTimeout(function() {
                        $btn.text('тык');
                    }, 3000);
                }
            });
        }
    });
</script>

</body>
</html>
