## На заметку

1. Готовые файлы лежат в dist (css уже импортирован в html).
2. Используется foundation email.
3. В mail.php лежит пример генерации письма.
4. [Тестовая форма для отправки письма](https://lit-river-46365.herokuapp.com/)
5. [Просмотр верстки](https://lit-river-46365.herokuapp.com/dist/index.html) 