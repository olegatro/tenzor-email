<?php

require __DIR__ . '/vendor/autoload.php';

use \Wa72\HtmlPageDom\HtmlPageCrawler;

if ($_POST['email']) {

    //mail
    $transport = Swift_SmtpTransport::newInstance('smtp.mailgun.org', 25)
        ->setUsername('postmaster@sandbox88564f2bde094fb2adaa6cea40296751.mailgun.org')
        ->setPassword('623e2a2a0b637d1a724c0f63162ff3ae');

    $mailer = Swift_Mailer::newInstance($transport);

    $subject = 'Тестовая рассылка| Tenzor';
    $to = trim($_POST['email']);

    $message = Swift_Message::newInstance($subject)
        ->setFrom(array('no-reply@olegatro.net'))
        ->setTo($to);

    //embed inline image
    $page = new HtmlPageCrawler(file_get_contents('dist/index.html'));

    $page
        ->filter('img')
        ->each(function ($node) use ($message) {
            $src = $node->attr('src');
            $cid = $message->embed(Swift_Image::fromPath('dist/' . $src));

            $node->attr('src', $cid);
        });

    /*
    //background image
    $page
        ->filter('.slider__inner, .discout__inner')
        ->each(function ($node) use ($message) {
            $bgImage = $node->css('background-image');

            if (preg_match('/\((.*)\)/', $bgImage, $m)) {
                $src = $m[1];
                $cid = $message->embed(Swift_Image::fromPath('dist/' . $src));

                $node->css('background-image', 'url(' . $cid . ')');
            }
        });
    */

    $message->setBody($page, 'text/html');


    $result = $mailer->send($message);

    $res['status'] = 'ok';
    $res['msg'] = 'ok';

    echo json_encode($res);
    die;
}
